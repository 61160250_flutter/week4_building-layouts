import 'package:flutter/material.dart';

Column _buildButtonColumn(Color color, IconData icon, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Icon(
        icon,
        color: color,
      ),
      Container(
        child: Text(
          label,
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      )
    ],
  );
}

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;

    Widget titileSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.only(
                    bottom: 8,
                  ),
                  child: Text(
                    'Oeschinen Lake Campground',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )),
              Text(
                'Kandersteg Switzerland',
                style: TextStyle(color: Colors.grey[500]),
              )
            ],
          )),
          Icon(
            Icons.star,
            color: Colors.red[500],
          ),
          Text('41')
        ],
      ),
    );

    Widget buttonSection = Container(

      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.call, 'CALL'),
          _buildButtonColumn(color, Icons.near_me, 'ROUTE'),
          _buildButtonColumn(color, Icons.share, 'SHARE'),
        ],
      ),
    );
    
    Widget textSection = Container(
         padding:  EdgeInsets.all(32),
         child: Text(
           'Lake Oeschinen lies at the foot. '
           'Guidtod guidtommm guidgataem. '
           'Half work Helf game. '
           'Prayut sonteen Kon-Cheers-Prayut goa sonteen. '
           'I need more money to upgrade this suck computer.'
           'I late to send homework teacher กบ because no money for buy better computer and some suck internet.'
           'Internet BUU still suck '
           'I surivive with kon-la-krueng with 1 meal per day because it save my money for order expenditure like electricity bill.'
           'My income from family not enough it income like 1500 bath per month. '
           'I try some work for good money for me but need to go outside regularly AND SOME F*CKING COVID!!!!.'
           'I need vaccine like Pizer not a shit Sicnovac. '
           'Better to move on hope teacher กบ understand my situation. '
         
         ,
         softWrap: true,
         ),
    );
    return MaterialApp(
      title: 'Flutter layout demo',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Flutter layout demo'),
        ),
        body: ListView(children: [
          Image.asset(
            'images/lake.jpg',
            width: 600,
            height: 240,
            fit: BoxFit.cover,
          ),
          titileSection,
          buttonSection,
          textSection,
        ]),
      ),
    );
  }
}
